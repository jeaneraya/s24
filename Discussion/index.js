// ES6 Updates:

// Exponent Operator:

// Exponent Operator: we use "**" for exponents

const firstNum = Math.pow(8,2);
console.log(firstNum);

const secondNum = 8 ** 2;
console.log(secondNum);


// 5 raised to the power of 5
const thirdNum = 5 ** 5;
console.log(thirdNum);


// Template Literals 

/*
	Allows us to write strings without using the concatination operator (+)
*/

let name = "George";

// Concatination / Pre-template literal
// Using single quote (' ')

let message = 'Hello' + name + 'Welcome to Zuitt Coding Bootcamp!';

console.log("Message without template " + message);

console.log("");

// Strings Using Template Literal
// Uses the backticks (` `)

message = `Hello ${name}. Welcome to Zuitt Coding Bootcamp!`;
console.log(`Message with template literal: ${message}`);

let anotherMessage = `${name} attended a math competition. He won it by solving the problem 8**2 with the solution of ${secondNum}`;

console.log(anotherMessage);

anotherMessage = "\n " + name + " attended a math competition. \n He won it by solving the problem 8**2 with the solution of " + secondNum + ". \n";

console.log(anotherMessage);

// Operation inside template literal
const interestRate = .1;
const principal = 1000;

console.log(`The ineterest on your savings is ${principal* interestRate}`);

// Array Destructuring

/*
	Allows to unpack elements in an array into distinct variables. Allows us to name the array elements with variables instead of index number

	Syntax: 
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Jean","Victoria","Eraya"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you`);


// Array Destruciting

const [firstName, secondName, lastName] = fullName;

console.log(firstName);
console.log(secondName);
console.log(lastName);

console.log(`Hello, ${firstName} ${secondName} ${lastName}! It's nice meeting you`);


// Object Destructuring

/*
	Allows to unpack properties of objects into distinct variable. Shortends the syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName, propertyName} = object
*/

const person = {
	fName: "Jean Victoria",
	mName: "Dangli",
	lName: "Eraya"
};


// Pre-object Destructuring
console.log(person.fName);
console.log(person.mName);
console.log(person.fName);

function getFullName(fname,mname,lname){
	console.log(`${fname} ${mname} ${lname}`);
};

getFullName(person.fName, person.mName,person.mNamemName);

// Using Object Destructuring
const {lName, fName, mName} = person;
console.log(lName);
console.log(fName);
console.log(mName);

function getFullName(fname,mname,lname){
	console.log(`${fname} ${mname} ${lname}`);
};

getFullName(fName,mName,lName);

const employees = [
			"Loisa Sheren",
			"Diego Lambog",
			"Francine Sembo",
			"Barbie Teroso"];

const [emp1,emp2,emp3,emp4] = employees;
console.log(`These are the names of the employees ${emp1}, ${emp2}, ${emp3}, ${emp4}.`);

const pet = {
		petName: "Doggy",
		breed: "Chihuahua",
		color: "white"
	};

const {petName,breed,color} = pet;
console.log(`My pet's name is ${petName}. He is a ${breed}. It's color is ${color}`);


// PRE-ARROW FUNCTION AND ARROW FUNCTION


// Pre-Arrow Function
/*
	Syntax:
		function functionName(paramA,paramB){
			statement; (console.log / return)
		}
*/

function printFullName(fName,mInitial,lName){
	console.log(fName + " " + mInitial + " " + lName);
}

printFullName("Trisha","D.","Parks");


// Arrow Function
/*
	Syntax:
		let/const variableName = (paramA,paramB) => {
			statement; (console.log / return)
		}
*/

const printMyFullName = (fName, mInitial,lName) => {
	console.log(`${fName} ${mInitial}. ${lName}`);
}

printMyFullName("Justine","Y","Timberlake");

const students = ["Jean","Ronel","Trisha"];

// FUNCTION WITH LOOP

// Pre-Arrow Function

students.forEach(function(student){
	console.log(student + " is a student");
});

// Arrow Function
console.log("");
students.forEach((student) => {console.log(`${student} is a student`)});


// Implicit Return Statement


//Pre-Arrow Function
function add(x,y){
	return x + y;
};

let total = add(12,15);
console.log(total);

// Arrow Function
const addition = (x,y) => x + y;

// use return keyword when curly braces is used

let total2 = addition(34,22);
console.log(total2);

//Default Function Argument Value

const greet = (name = "User") => {
	return `Good evening, ${name}`;
}

console.log(greet());

// Class-Based Object Blueprint
/*
	Allows creation/instantiation of objects using class as blueprint

	Syntax:
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA,
				this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car {
	constructor(brand,name,year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
}

const car = new Car();
console.log(car);

car.brand = "Ford";
car.name = "Ranger Raptor";
car.year = 2021;

console.log(car);

const anotherCar = new Car("Toyota","Vios",2020);
console.log(anotherCar);