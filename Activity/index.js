

// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:

		const num = 8
		const getCube = num ** 3;
		console.log(`The cube of ${num} is ${getCube}`);

	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:

		const address = ["Sabel St.", "Kidapawan City","North Cotabato"];
		console.log(address);

		const [street,city,province] = address;
		console.log(`Teresa is living in ${street}, ${city}, ${province}.`);

	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:

		const animal = {
			animalName: "Dog",
			breed: "Chihuahua",
			color: "White"
		};

		const {animalName,breed,color} = animal;

		console.log(`I have selected a ${animalName}. He is a ${breed}. It's color is ${color}.`);


	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:

		const numbers = [1,2,3,4,5];
		numbers.forEach((number) => console.log(number) );

/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:

		class Dog {
			constructor(name,age,breed){
				this.name = name,
				this.age = age,
				this.breed = breed
			}
		}

		const myDog = new Dog("chibi",5,"Husky");
		console.log(myDog);